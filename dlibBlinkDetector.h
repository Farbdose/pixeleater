#pragma once

#include "AbstractBlinkDetector.h"

#include <opencv2/dnn.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/video/tracking.hpp>

#include <dlib/global_optimization.h>
#include <dlib/image_processing.h>
#include <dlib/image_processing/frontal_face_detector.h>
#include <dlib/opencv/cv_image.h>
#include <dlib/optimization.h>

namespace BlinkDetector {

    typedef dlib::matrix<double, 0, 1> column_vector;

    float getNewDataPointWrapper2(const column_vector &m);

    void dlib_point2cv_Point(dlib::full_object_detection &S, std::vector<cv::Point> &L);

    class DlibBlinkDetector : public AbstractBlinkDetector {
    public:
        explicit DlibBlinkDetector(
                const QString &settingsPath);

    protected:
        dlib::frontal_face_detector m_detector;
        dlib::shape_predictor m_posemodel;
        int m_alpha;
        int m_beta;
        int m_darkness;
        int m_darkest;
        int m_brightness;

        bool findFace(cv::Mat frame) override;

        void findEyes() override;

        void loadModels() override;

        void calcEyeState() override;

        // TODO move into separate class between abstract and here
        double colorBased(cv::Mat &frame, std::vector<cv::Point> &landmarks);

        double absColorBased(cv::Mat &frame);

        double circleBased(cv::Mat &frame, std::vector<cv::Point> &shape, bool lowLight);

        double edgeBased(cv::Mat &frame, std::vector<cv::Point> &shape, bool lowLight);
    };
}
