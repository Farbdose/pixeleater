//
// Created by fabian on 06.04.19.
//

#include "CameraControl.h"

BlinkDetector::CameraControl::CameraControl(const int &camera_fd)
        : contrast(camera_fd, V4L2_CID_CONTRAST), gamma(camera_fd, V4L2_CID_GAMMA), gain(camera_fd, V4L2_CID_GAIN),
          exposure(camera_fd, V4L2_CID_EXPOSURE_ABSOLUTE) {

    v4l2_control control{};
    control.id = V4L2_CID_EXPOSURE_AUTO;
    control.value = V4L2_EXPOSURE_MANUAL;
    ioctl(camera_fd, VIDIOC_S_CTRL, &control);

    exposure.setMaximum(500);
    exposure.setMinimum(10);
}
