#include "pixeleater.h"

namespace PixelEater {

    QtPixelEater::QtPixelEater(QScreen *scr, BlinkDetector::AbstractBlinkDetector *detector,
                               const QString &settingsPath)
            : m_counter(0), m_blinkData(), m_eyeTimeTotal(0), m_videoStream(QPixmap()), m_background(QPixmap()),
              m_flash_black(QPixmap()), m_flash_white(QPixmap()), m_statusText(), m_eyeTime(0), m_breakTimeDone(0),
              m_breakTimeRequired(0), m_breakTimeLeft(0), m_ticksPerSec(20), m_secondsSinceEyesClosed(0),
              m_settings(settingsPath, QSettings::IniFormat) {
        m_devMode = m_settings.value("devMode", false).toBool();
        m_settings.beginGroup("pixeleater");
        m_ticksPerSec = m_settings.value("tps", 20).toInt();
        m_timer.setInterval(1000 / m_ticksPerSec);
        m_screen = scr;

        connect(&m_timer, &QTimer::timeout, this, &QtPixelEater::tick);
        m_detector = detector;
        setup();
    }

    void QtPixelEater::setup() {
        setAttribute(Qt::WA_ShowWithoutActivating);
        setAttribute(Qt::WA_TransparentForMouseEvents, true);
        setAttribute(Qt::WA_TranslucentBackground);
        setStyleSheet("QWidget{background-color:transparent}");

        setWindowFlags(
                Qt::Tool | Qt::WindowTransparentForInput | Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint |
                Qt::CustomizeWindowHint | Qt::X11BypassWindowManagerHint);
        setFocusPolicy(Qt::NoFocus);
        setupScene();
    }

    void QtPixelEater::setupScene() {
        this->setScene(new QGraphicsScene(this));
        this->scene()->addItem(&m_videoStream);
        this->setFixedSize(m_screen->size().width(), m_screen->size().height());
        this->setSceneRect(0, 0, m_screen->size().width(), m_screen->size().height());
        this->setFrameStyle(0);
        this->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        this->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

        QPixmap qb(m_screen->size().width(), m_screen->size().height());
        qb.fill(QColor(0, 0, 0, 255));
        m_flash_black.setPixmap(qb);
        m_flash_black.hide();
        this->scene()->addItem(&m_flash_black);

        QPixmap qw(m_screen->size().width(), m_screen->size().height());
        qw.fill(QColor(255, 255, 255, 255));
        m_flash_white.setPixmap(qw);
        m_flash_white.hide();
        this->scene()->addItem(&m_flash_white);

        m_background.setPos(0, m_screen->size().height() - 24 * (m_devMode ? 2 : 1));
        this->scene()->addItem(&m_background);

        this->scene()->addItem(&m_statusText);
        m_statusText.setPos(0, m_screen->size().height() - 24 * (m_devMode ? 2 : 1));
        m_statusText.setDefaultTextColor(QColor(255, 0, 0, 180));
        QFont font = QFontDatabase::systemFont(QFontDatabase::FixedFont);
        font.setPixelSize(16);
        font.setBold(true);
        font.setStyleStrategy(QFont::PreferAntialias);
        m_statusText.setFont(font);

        m_timer.start();

        // usleep(1000 * 1000);
        // QtConcurrent::run(this, &QtPixelEater::flash);
    }

    void QtPixelEater::tick() {

        bool changes = m_detector->copyData(&m_blinkData);

        QPixmap qp(m_blinkData.trackEyes ? 644 : 277, 24);
        qp.fill(QColor(0, 0, 0, 140));
        m_background.setPixmap(qp);

        if (m_blinkData.faceDetected && (!m_blinkData.trackEyes || m_blinkData.eyesOpen)) {
            if (m_flash_black.isVisible()) {
                m_flash_black.hide();
            }

            if (m_flash_white.isVisible()) {
                m_flash_white.hide();
            }
        }

        if (m_counter >= m_ticksPerSec) {
            m_counter = 0;
            changes = true;

            if (m_blinkData.eyesDetected && m_blinkData.eyesOpen) {
                m_secondsSinceEyesClosed = 0;
                m_eyeTimeTotal++;
                m_eyeTime++;
                m_breakTimeRequired = calcBreakTime(m_eyeTime);
                // old: m_eyeTime / 3.0 * std::pow(1.0002, m_eyeTime); // be aware that these count in seconds;
            } else if ((m_blinkData.eyesDetected && !m_blinkData.eyesOpen) or !m_blinkData.faceDetected) {
                m_secondsSinceEyesClosed++;
            }

            if (m_secondsSinceEyesClosed > 1 && m_breakTimeRequired > 2 && m_breakTimeRequired > 60 * 0.5) {
                m_breakTimeDone += 1;
            }

            m_breakTimeLeft = m_breakTimeRequired - m_breakTimeDone;

            // required accumulated break time done -> reset
            if (m_breakTimeLeft < 0) {
                if (m_breakTimeRequired > 10) {
                    QtConcurrent::run(this, &QtPixelEater::flash);
                }

                m_eyeTime = 0;
                m_breakTimeDone = 0;
                m_breakTimeLeft = 0;
                m_breakTimeRequired = 0;
            }
        }

        if (changes || m_blinkData.faceDetected /*|| (m_secondsSinceEyesClosed > 1 && m_breakTime > 2)*/) {
            QString text;
            m_videoStream.setPixmap(QPixmap());
            if (m_blinkData.trackEyes) {
                text.sprintf("FPS=%2i  Blinks=%4i (every %3.1fs)  -  Eye %3i:%02i/%3i:%02i/%2i:%02i Break",
                             m_blinkData.fps,
                             m_blinkData.blinks,
                             m_blinkData.avgBlinkTime,
                             (int) floor((m_eyeTimeTotal) / 60),
                             (int) floor(m_eyeTimeTotal) % 60,
                             (int) floor((m_eyeTime) / 60),
                             (int) floor(m_eyeTime) % 60,
                             (int) floor((m_breakTimeLeft) / 60),
                             (int) floor(m_breakTimeLeft) % 60);
            } else {
                text.sprintf("FPS=%2i - %03i:%02i/%03i:%02i/%02i:%02i",
                             m_blinkData.fps,
                             (int) floor((m_eyeTimeTotal) / 60),
                             (int) floor(m_eyeTimeTotal) % 60,
                             (int) floor((m_eyeTime) / 60),
                             (int) floor(m_eyeTime) % 60,
                             (int) floor((m_breakTimeLeft) / 60),
                             (int) floor(m_breakTimeLeft) % 60);
            }

            if (!m_blinkData.faceDetected ||
                (m_blinkData.enableFiveMinBreak && m_breakTimeLeft > 60 * 0.5 && m_breakTimeLeft < 60 * 0.5 + 1) ||
                (m_breakTimeLeft > 60 * 2 && m_breakTimeLeft < 60 * 2 + 5) ||
                (m_breakTimeLeft > 60 * 5 && m_breakTimeLeft < 60 * 5 + 10) ||
                (m_breakTimeLeft >= 60 * 10 && (static_cast<int>(m_breakTimeLeft) % (60 * 10) > 0) &&
                 (static_cast<int>(m_breakTimeLeft) % (60 * 10) < 30))) {
                cv::Mat frame = m_detector->m_frameBaseInputScaled;
                QImage qimg(frame.data, frame.cols, frame.rows, frame.step, QImage::Format_Grayscale8);
                m_videoStream.setPixmap(QPixmap::fromImage(qimg.rgbSwapped()));
                m_videoStream.setOpacity(0.9);
            }

            m_statusText.setPlainText(text);
        }

        m_counter++;
    }

/**
     * calculates required breaktime based on screentime
     * @param x screentime in seconds
     * @return breaktime in seconds
     */
    double QtPixelEater::calcBreakTime(double x) {
        x /= 60;

        if (x < 0) {
            return 0;
        } else if (x < 5) {
            return (0.1 * x) * 60;
        } else if (x < 15) {
            return (0.0333333 * x + 0.01 * x * x + 0.000666667 * x * x * x) * 60;
        }

        return (x - 10) * 60;
    }

    void QtPixelEater::flash() {
        int d = 500 * 1000;
        m_flash_black.show();

        usleep(d);

        for (int i = 0; i < 3 && !m_blinkData.faceDetected; i++) {
            m_flash_white.show();
            usleep(d);

            m_flash_white.hide();
            usleep(d);
        }

        // m_flash_black.hide();
    }
}
