//
// Created by fabian on 06.04.19.
//
#pragma once

#include <algorithm>
#include <cmath>
#include <linux/videodev2.h>
#include <sys/ioctl.h>

namespace BlinkDetector {

    class CamControlIo {
    protected:
        v4l2_queryctrl m_query;
        v4l2_control m_ctrl;
        int m_camera_fd;
        int m_precission;

    public:
        CamControlIo(const int &camera_fd, __u32 id);

        explicit operator float() const;

        explicit operator double() const;

        CamControlIo &operator*=(const float &value);

        CamControlIo &operator/=(const float &value);

        CamControlIo &operator-=(const float &value);

        CamControlIo &operator+=(const float &value);

        bool operator<(const float &value);

        bool operator>(const float &value);

        bool operator==(const float &value);

        bool operator<(const CamControlIo &value);

        bool operator>(const CamControlIo &value);

        bool operator==(const CamControlIo &value);

        CamControlIo &operator++();

        CamControlIo &operator--();

        CamControlIo operator++(int x) &;

        CamControlIo operator--(int x) &;

        float operator-(const float &value);

        float operator+(const float &value);

        CamControlIo &operator=(const float &value);

        CamControlIo() = default;

        int getMin() const;

        int getMax() const;

        int getDef() const;

        int getValAbs() const;

        float getRelativeStep() const;

        float getVal() const;

        void setValAbs(int value);

        void setMaximum(int value);

        void setMinimum(int value);
    };

    float operator-(float x, const CamControlIo &y);

    float operator+(float x, const CamControlIo &y);
}
