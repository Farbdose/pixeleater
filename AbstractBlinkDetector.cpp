//
// Created by fabian on 23.03.19.
//

#include "dlibBlinkDetector.h"

BlinkDetector::AbstractBlinkDetector *objRef;

int BlinkDetector::limitIntToRange(int min, int num, int max) {
    return num < min ? min : (num > max ? max : num);
}

void BlinkDetector::limitRectToFrame(cv::Rect &rect, const cv::Mat &frame) {
    int w = frame.size().width;
    int h = frame.size().height;
    rect.x = limitIntToRange(0, rect.x, w);
    rect.y = limitIntToRange(0, rect.y, h);
    rect.width = limitIntToRange(0, rect.width, w - rect.x);
    rect.height = limitIntToRange(0, rect.height, h - rect.y);
}

void BlinkDetector::dlib_point2cv_Point(dlib::full_object_detection &S, std::vector<cv::Point> &L) {
    for (unsigned long i = 0; i < S.num_parts(); ++i) {
        L.emplace_back(S.part(i).x(), S.part(i).y());
    }
}

cv::Scalar BlinkDetector::getMeanOfShape(cv::Mat &frame, std::vector<cv::Point> &shape) {
    cv::Point pts[1][shape.size()];
    for (size_t i = 0; i < shape.size(); i++) {
        pts[0][i] = cv::Point(shape[i].x, shape[i].y);
    }
    const cv::Point *points[1] = {pts[0]};
    int npoints = static_cast<int>(shape.size());

    cv::Mat1b mask(frame.rows, frame.cols, uchar(0));
    cv::fillPoly(mask, points, &npoints, 1, cv::Scalar(255));
    return cv::mean(frame, mask);
}

int BlinkDetector::roundToNextMultipleOf(double num, bool down, int grid) {
    return (int) ((down ? floor(num / grid) : ceil(num / grid)) * grid);
}

void BlinkDetector::scaleCvRect(cv::Rect &rect, double scale, bool fromCenter) {
    if (fromCenter) {
        rect.x = static_cast<int>(rect.x - (rect.width / 2.0) * (scale - 1));
        rect.y = static_cast<int>(rect.y - (rect.height / 2.0) * (scale - 1));
        rect.width = static_cast<int>(rect.width * scale);
        rect.height = static_cast<int>(rect.height * scale);
    } else {
        rect.x = static_cast<int>(rect.x * scale);
        rect.y = static_cast<int>(rect.y * scale);
        rect.width = static_cast<int>(rect.width * scale);
        rect.height = static_cast<int>(rect.height * scale);
    }
}

cv::Rect BlinkDetector::projectRectOutOfParent(const cv::Rect &rect, const cv::Rect &parent) {
    return cv::Rect(
            parent.x + rect.x,
            parent.y + rect.y,
            rect.width,
            rect.height);
}

/**
 * https://stackoverflow.com/a/24341809/2422125
 * @param input
 * @return
 */
cv::Mat BlinkDetector::adaptiveHistogramEqualization(const cv::Mat &input, int clipLimit = 4, int tileSize = 8) {
    cv::Mat lab_image;
    cv::cvtColor(input, lab_image, cv::COLOR_RGB2Lab);

    // Extract the L channel
    std::vector<cv::Mat> lab_planes(3);
    cv::split(lab_image, lab_planes); // now we have the L image in lab_planes[0]

    // apply the CLAHE algorithm to the L channel
    cv::Ptr<cv::CLAHE> clahe = cv::createCLAHE(clipLimit, cv::Size(tileSize, tileSize));
    cv::Mat dst;
    clahe->apply(lab_planes[0], dst);

    // Merge the the color planes back into an Lab image
    dst.copyTo(lab_planes[0]);
    cv::merge(lab_planes, lab_image);

    // convert back to RGB
    cv::Mat image_clahe;
    cv::cvtColor(lab_image, image_clahe, cv::COLOR_Lab2RGB);

    return image_clahe;
}

cv::Mat BlinkDetector::equalizeIntensity(const cv::Mat &inputImage) {
    cv::Mat result = inputImage.clone();
    equalizeIntensityInplace(result);

    return result;
}

void BlinkDetector::equalizeIntensityInplace(const cv::Mat &inputImage) {
    switch (inputImage.channels()) {
        case 1:
            equalizeHist(inputImage, inputImage);
            break;
        case 2: {
            std::vector<cv::Mat> channels;
            split(inputImage, channels);
            equalizeHist(channels[0], channels[0]);
        };
            break;
        default: {
            cvtColor(inputImage, inputImage, cv::COLOR_BGR2YCrCb);

            std::vector<cv::Mat> channels;
            split(inputImage, channels);
            equalizeHist(channels[0], channels[0]);
            merge(channels, inputImage);

            cvtColor(inputImage, inputImage, cv::COLOR_YCrCb2BGR);
        }
            break;
    }
}

BlinkDetector::AbstractBlinkDetector::AbstractBlinkDetector(const QString &settingsPath)
        : m_running(true), m_frameRaw(), m_frameCropped(), m_frameLeftEye(), m_frameRightEye(), m_frameEyeArea(),
          m_eyeAreaTimeShift(), m_frameEyeAreaSmooth(), m_utex(), m_blinkData(), m_batchSize(100), m_trackEyes(true),
          m_showFrames(true), m_maxFPS(30), m_num(3), m_enableFiveMinBreak(false),
          m_settings(settingsPath, QSettings::IniFormat) {
    m_devMode = m_settings.value("devMode", false).toBool();
    m_settings.beginGroup("blinkdetector");

    m_maxFPS = m_settings.value("fps", 30).toInt();

    objRef = this;

    m_showFrames = m_settings.value("showFrames", "true").toBool();
    m_trackEyes = m_maxFPS >= 15 ? m_settings.value("trackEyes", "true").toBool() : false;

    m_ainLoop = std::thread(&AbstractBlinkDetector::run, this);
    pthread_setname_np(m_ainLoop.native_handle(), "BlinkDetector");
}

void BlinkDetector::AbstractBlinkDetector::stop() {
    m_running = false;
    m_ainLoop.join();
}

void BlinkDetector::AbstractBlinkDetector::resetCrop() {
    m_cropRect.x = 0;
    m_cropRect.y = 0;
    m_cropRect.width = m_frameRaw.size().width;
    m_cropRect.height = m_frameRaw.size().height;
    m_faceRect.x = 0;
    m_faceRect.y = 0;
    m_faceRect.width = m_frameRaw.size().width;
    m_faceRect.height = m_frameRaw.size().height;
}

double BlinkDetector::EAR(std::vector<cv::Point> &landmarks) {
    double p38_40_dist = cv::norm(landmarks[38] - landmarks[40]);
    double p37_41_dist = cv::norm(landmarks[37] - landmarks[41]);
    double p36_39_dist = cv::norm(landmarks[36] - landmarks[39]);
    double EAR1 = (p37_41_dist + p38_40_dist) / (2 * p36_39_dist);

    double p44_46_dist = cv::norm(landmarks[44] - landmarks[46]);
    double p43_47_dist = cv::norm(landmarks[43] - landmarks[47]);
    double p42_45_dist = cv::norm(landmarks[42] - landmarks[45]);
    double EAR2 = (p43_47_dist + p44_46_dist) / (2 * p42_45_dist);

    return (EAR1 + EAR2) / 2;
}

cv::Mat BlinkDetector::toGray(const cv::Mat &src) {
    if (src.type() == CV_8UC1)
        return src;

    cv::Mat gray;
    if (src.type() == CV_8UC2)
        cvtColor(src, gray, cv::COLOR_YUV2GRAY_YVYU);
    else if (src.type() == CV_8UC3)
        cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    else if (src.type() == CV_8UC4)
        cvtColor(src, gray, cv::COLOR_BGRA2GRAY);
    return gray;
}

cv::UMat BlinkDetector::toGray(const cv::UMat &src) {
    if (src.type() == CV_8UC1)
        return src;

    cv::UMat gray;
    if (src.type() == CV_8UC3)
        cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    else if (src.type() == CV_8UC4)
        cvtColor(src, gray, cv::COLOR_BGRA2GRAY);
    return gray;
}

/**
 * based on http://answers.opencv.org/question/75510/how-to-make-auto-adjustmentsbrightness-and-contrast-for-image-android-opencv-image-correction/?answer=75797#post-id-75797
 *  \brief Automatic brightness and contrast optimization with optional histogram clipping
 *  \param [in]src Input image GRAY or BGR or BGRA
 *  \param [out]dst Destination image
 *  \param clipHistPercent cut wings of histogram at given percent tipical=>1, 0=>Disabled
 *  \note In case of BGRA image, we won't touch the transparency
*/
void BrightnessAndContrastAuto(const cv::Mat &src, int *contrast, int *exposure, int *gamma, int clipHistPercent = 5) {

    CV_Assert(clipHistPercent >= 0);
    CV_Assert((src.type() == CV_8UC1) || (src.type() == CV_8UC3) || (src.type() == CV_8UC4));

    int histSize = 256;
    float alpha, beta;
    double minGray = 0, maxGray = 0;

    //to calculate grayscale histogram
    cv::Mat gray = BlinkDetector::toGray(src);

    if (clipHistPercent == 0) {
        // keep full available range
        cv::minMaxLoc(gray, &minGray, &maxGray);
    } else {
        cv::Mat hist; //the grayscale histogram

        float range[] = {0, 256};
        const float *histRange = {range};
        bool uniform = true;
        bool accumulate = false;
        calcHist(&gray, 1, 0, cv::Mat(), hist, 1, &histSize, &histRange, uniform, accumulate);

        // calculate cumulative distribution from the histogram
        std::vector<float> accumulator(histSize);
        accumulator[0] = hist.at<float>(0);
        for (int i = 1; i < histSize; i++) {
            accumulator[i] = accumulator[i - 1] + hist.at<float>(i);
        }

        // locate points that cuts at required value
        float max = accumulator.back();
        float clipHistPercent2 = clipHistPercent * (max / 100.0); //make percent as absolute
        clipHistPercent2 /= 2.0;                                  // left and right wings
        // locate left cut
        minGray = 0;
        while (accumulator[minGray] < clipHistPercent2)
            minGray++;

        // locate right cut
        maxGray = histSize - 1;
        while (accumulator[maxGray] >= (max - clipHistPercent2))
            maxGray--;
    }

    // current range
    double inputRange = maxGray - minGray;
    double targetRange = histSize - 20;

    if (inputRange < targetRange - 5) {
        *contrast += abs(inputRange - targetRange) > 10 ? 1 : 1;
    } else if (inputRange > targetRange + 5) {
        *contrast -= abs(inputRange - targetRange) > 10 ? 1 : 1;
    }

    int ceiling = 245;
    int maxExposure = 412;

    if (maxGray < ceiling - 5 && minGray < 90) {
        if (*exposure < 10 || (*exposure < maxExposure)) {
            *exposure += 10;
        } else if (*exposure == maxExposure) {
            *gamma += 5;
        }
    } else if (maxGray > ceiling + 5) {
        if (*gamma > /*72*/ 48) {
            *gamma -= 5;
        } else {
            *exposure -= 10;
        }
    }

    *contrast = std::max(0, std::min(/*64*/ 95, *contrast));
    *exposure = std::max(0, std::min(maxExposure, *exposure));
    *gamma = std::max(/*72*/ 48, std::min(250, *gamma));

    // std::cout << minGray << "|" << maxGray << "|" << inputRange << "|" << targetRange << "|" << *exposure << "|"
    //          << *contrast << "|" << *gamma << std::endl;
}

void BlinkDetector::AbstractBlinkDetector::calcHist(const cv::UMat &src, int histSize = 256) {

    std::vector<float> range = {0.0f, static_cast<float>(histSize)};

    auto histS = {histSize};
    const int totalPixels = src.cols * src.rows;

    cv::UMat dst3;
    std::vector<cv::UMat> in = {src};
    std::vector<int> channels = {0};

    cv::calcHist(in, channels, cv::UMat(), dst3, histS, range, false);
    dst3 = dst3.mul(cv::Scalar::all(1.0f / totalPixels));

    cv::Mat dst;
    dst3.copyTo(dst);

    cv::Mat dst2 = dst.clone();
    for (int i = 1; i < histSize - 1; i++) {
        float prev = dst.at<float>(i - 1);
        float next = dst.at<float>(i + 1);

        if ((i < histSize / 2 && i % 7 == 1) || (i >= histSize / 2 && i % 7 == 0)) {
            dst2.at<float>(i) = (prev + next) / 2.0f;
        }
    }
    dst = dst2.clone();

    cv::GaussianBlur(dst, dst, cv::Size(1, 9), 0, 0);

    for (int i = 0; i < histSize; i++) {
        if (dst.at<float>(i) < 0.0005) {
            dst.at<float>(i) = 0;
        }
    }

    m_hist.hist = dst;
    m_hist.input = cv::Mat(1, 9, cv::DataType<float>::type);
    m_hist.output = cv::Mat(1, 4, cv::DataType<float>::type);

    int i = 0;
    for (i = 0; i < histSize && dst.at<float>(static_cast<int>(i)) < 0.00000001; i++) {
    }
    m_hist.input[0][1] = static_cast<float>(i) / histSize;

    for (i = histSize - 1; i > 0 && dst.at<float>(static_cast<int>(i)) < 0.00000001; i--) {
    }
    m_hist.input[0][3] = 1 - static_cast<float>(i) / histSize;

    m_hist.input[0][2] = dst.at<float>(0) * 10.0f;
    m_hist.input[0][4] = dst.at<float>(static_cast<int>(histSize - 1)) * 10.0f;
    m_hist.input[0][0] = m_hist.input[0][3] - m_hist.input[0][1] + m_hist.input[0][2] - m_hist.input[0][4];
    m_hist.input[0][5] = (float) (m_cam.contrast);
    m_hist.input[0][6] = (float) (m_cam.gamma);
    m_hist.input[0][7] = (float) (m_cam.gain);
    m_hist.input[0][8] = (float) (m_cam.exposure);

    auto currentExp = std::max(0.01f, static_cast<float>(m_cam.exposure));
    float expChange =
            m_cam.exposure < 1 ? (m_hist.input[0][0] * 0.3f + 1.0f) * currentExp - currentExp : m_hist.input[0][0] *
                                                                                                0.15f;
    float conChange = 0.3f * (m_hist.input[0][1] - m_hist.input[0][2] + m_hist.input[0][3] - m_hist.input[0][4]);

    if ((expChange > 0 && m_cam.gamma == 1) || (expChange < 0 && m_cam.exposure == 0)) {
        expChange = 0;
    }

    if (((conChange > 0 && (m_cam.gain == 1 || (m_cam.contrast == 1 && m_cam.exposure < 1))) ||
         (conChange < 0 && m_cam.contrast == 0))) {
        conChange = 0;
    }

    m_hist.error = abs(m_hist.input[0][1] - m_hist.input[0][2] + m_hist.input[0][3] - m_hist.input[0][4])
                   + abs(m_hist.input[0][3] - m_hist.input[0][1] + m_hist.input[0][2] - m_hist.input[0][4]);

    if (!m_devMode) {
        if (expChange > 0) {
            if (m_cam.exposure < 1) {
                m_cam.exposure += 3 * expChange;
            } else {
                m_cam.gamma += expChange;
            }
        } else if (expChange < 0) {
            if (m_cam.gamma > 0) {
                m_cam.gamma += expChange;
            } else {
                m_cam.exposure += 3 * expChange;
            }
        }

        if (m_cam.gamma > 0 && m_cam.exposure < 1) {
            m_cam.gamma -= 1 - m_cam.exposure;
            m_cam.exposure = 1;
        }

        if (conChange > 0) {
            if (m_cam.contrast < 1) {
                m_cam.contrast += conChange;
            } else {
                m_cam.gain += conChange;
            }
        } else if (conChange < 0) {
            if (m_cam.gain > 0) {
                m_cam.gain += conChange;
            } else {
                m_cam.contrast += conChange;
            }
        }

        if (m_cam.gain > 0 && m_cam.contrast < 1) {
            m_cam.gain -= 1 - m_cam.contrast;
            m_cam.contrast = 1;
        }

        // fallback to keep gain always off while exposure < 1
        if (m_cam.gain > 0 && m_cam.exposure < 1) {
            m_cam.gain = 0;
        }
    }
}

void BlinkDetector::AbstractBlinkDetector::showHist(const cv::UMat &src) {
    int histSize = 256;

    //cv::Mat grey = toGray(src);
    calcHist(src, histSize);
    /*
    std::cout << std::fixed << std::setprecision(3);
    std::cout << m_hist.error << " | ";
    for (int i = 0; i < 5; i++)
        std::cout << m_hist.input[0][i] << " ";
    std::cout << "| ";
    for (int i = 5; i < 9; i++)
        std::cout << m_hist.input[0][i] << " ";

    std::cout << std::endl;*/

    int hist_w = 1024, hist_h = 512;
    int bin_w = static_cast<int>(ceil((double) hist_w / histSize));

    cv::Mat histImage(hist_h, hist_w, CV_8UC3, cv::Scalar(0));
    m_hist.hist *= hist_h * 20;

    for (int i = 0; i < histSize - 1; i++) {
        line(histImage, cv::Point(bin_w * i, hist_h - cvRound(m_hist.hist.at<float>(i))),
             cv::Point(bin_w * (i + 1), hist_h - cvRound(m_hist.hist.at<float>(i + 1))), cv::Scalar(255, 255, 255), 1,
             8, 0);
    }

    if (m_showFrames) {
        QString text1("Contrast Gain Gamma Exposure     LeftHeight LeftDist RightDist RightHeight     Skewness Error");

        QString text2;
        text2.sprintf("%8.2f %4.2f %5.2f %8.3f     %10.2f %8.2f %9.2f %11.2f     %8.1f %5.2f",
                      m_hist.input[0][5],
                      m_hist.input[0][7],
                      m_hist.input[0][6],
                      m_hist.input[0][8],
                      m_hist.input[0][2],
                      m_hist.input[0][1],
                      m_hist.input[0][3],
                      m_hist.input[0][4],
                      m_hist.input[0][0],
                      m_hist.error

        );

        int lineHeight = 16;
        cv::addText(histImage, text1.toStdString(), cv::Point2f(8, 4 + 1 * lineHeight), "Monospace", 8,
                    cv::Scalar(255, 255, 255));
        cv::addText(histImage, text2.toStdString(), cv::Point2f(8, 4 + 2 * lineHeight), "Monospace", 8,
                    cv::Scalar(255, 255, 255));
        imshow("Camera Histogram", histImage);
    }
}

void BlinkDetector::toggleButtonCallback(int state, void *data) {
    auto dataBool = reinterpret_cast<bool *>(data);
    *dataBool = !(*dataBool);
}

void BlinkDetector::AbstractBlinkDetector::run() {
    std::cout << "Loading..." << std::endl;
    m_num = 1;

    m_hist.output = cv::Mat(1, 4, cv::DataType<float>::type);
    m_hist.input = cv::Mat(1, 9, cv::DataType<float>::type);

    double tt_opencvDNN = 0;
    double fpsOpencvDNN = 0;
    bool eyesClosed = false;
    int blinks = 0;
    bool streamIsOk = false;
    auto lastBlinkAt = std::chrono::high_resolution_clock::now();
    double avgBlinkTime = 5;
    double t = cv::getTickCount();
    double fps = 10;
    int framesSinceFace = 0;

    if (m_showFrames) {
        cv::namedWindow("Camera Histogram", cv::WINDOW_GUI_NORMAL | cv::WINDOW_AUTOSIZE);
        std::string fiveMinBreakButtonLabel = "enable 5 minute break";
        cv::createButton(fiveMinBreakButtonLabel, toggleButtonCallback, &m_enableFiveMinBreak,
                         cv::QtButtonTypes::QT_CHECKBOX, 1);

        if (m_trackEyes) {
            cv::namedWindow("frameLeftEye");
            cv::resizeWindow("frameLeftEye", 150, 50);

            cv::namedWindow("frameRightEye");
            cv::resizeWindow("frameRightEye", 150, 50);
        }
    }

    loadModels();

    cv::Mat tmp;

    int camera_fd;

    camera_fd = open("/dev/video0", O_RDWR);

    // works
    m_cam = CameraControl(camera_fd);

    // exposure time is measured in 0.1ms steps -> exposure = 1000 means 100ms per frame
    m_cam.exposure.setMaximum(std::min(m_settings.value("maxExposure", 3000).toInt(), 1000 * 10 / m_maxFPS));

    /*for (int i = 0; i < m_optiRuns; i++) {
        runOptimizer();
    }*/

    std::cout << "Starting main Blink Loop" << std::endl;

    for (int i = 0; i < m_histBatch.input.rows; i++) {
        for (int j = 0; j < m_histBatch.input.cols; j++) {
            std::cout << m_histBatch.input.at<float>(i, j) << " ";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << m_histBatch.output << std::endl
              << std::endl
              << std::endl;

    int framesSinceHist = 0;
    std::chrono::high_resolution_clock::time_point lastHistTime;

    double colorSpaceCorrection = 255.0 / m_settings.value("videoSourceMaxBrightness", 255.0).toDouble();
    while (m_running) {
        auto beginFrame = std::chrono::high_resolution_clock::now();
        checkSource();

        bool frameReceived = m_source.read(tmp);
        if (frameReceived) {
            if (colorSpaceCorrection != 1) {
                tmp = tmp.mul(cv::Scalar::all(colorSpaceCorrection));
            }

            toGray(tmp).copyTo(m_frameRaw);

            m_showFrames = m_settings.value("showFrames", "true").toBool();
            m_trackEyes = m_settings.value("trackEyes", "true").toBool();
            m_grid = (int) (std::min(m_frameRaw.size().width, m_frameRaw.size().height) *
                            m_settings.value("grid", "0.1").toDouble());
            m_inShift = (int) (std::min(m_frameRaw.size().width, m_frameRaw.size().height) *
                               m_settings.value("minShift", "0.01").toDouble());
            m_grid = std::max(1, m_grid);
            m_inputCropScale = m_settings.value("inputCropScale", "1.6").toDouble();

            if (m_frameRaw2.empty()) {
                m_frameRaw.copyTo(m_frameBaseInput);
                m_frameBaseInputMean = m_frameRaw.clone();
                m_frameRaw2 = m_frameRaw.clone();
                m_frameRaw3 = m_frameRaw.clone();

                m_frameRaw.copyTo(m_frameFace);
                m_frameRaw.copyTo(m_frameCropped);

                resetCrop();
            }

            //        m_frameBaseInput = (m_frameRaw3 - m_frameRaw;
            //cv::absdiff(m_frameRaw,m_frameRaw2, m_frameRaw3FDiff);
            m_frameRaw3 = m_frameRaw2;
            m_frameRaw2 = m_frameRaw.clone();

            // cv::cuda::GpuMat dst, src
            // src.upload(m_frameRaw);

            cv::UMat z;
            if (m_maxFPS >= 30) {
                z = m_frameRaw.mul(cv::Scalar::all(0.33));
                cv::add(z, m_frameRaw2.mul(cv::Scalar::all(0.33)), z);
                cv::add(z, m_frameRaw3.mul(cv::Scalar::all(0.33)), z);
            } else if (m_maxFPS >= 15) {
                z = m_frameRaw.mul(cv::Scalar::all(0.5));
                cv::add(z, m_frameRaw2.mul(cv::Scalar::all(0.5)), z);
            } else {
                z = m_frameRaw;
            }

            cv::medianBlur(z, z, 3);
            float p = 0; // 0.275 + 0.0125 * std::max(14, std::min(25, m_maxFPS));
            cv::add(m_frameBaseInputMean.mul(cv::Scalar::all(p)), z.mul(cv::Scalar::all(1 - p)), m_frameBaseInputMean);

            // std::vector<cv::UMat> timeShift = {m_frameRaw3, m_frameRaw2, m_frameRaw};
            // cv::fastNlMeansDenoisingColoredMulti(timeShift, m_frameBaseInputMean,1,3,10.0,10.0,7,21);

            //m_frameBaseInputMean = m_frameRaw;
            m_frameBaseInputMean.copyTo(m_frameBaseInput); //m_frameRaw2.clone();
            m_frameBaseInputMeanGrey = toGray(m_frameBaseInputMean);

            m_frameCropped = m_frameBaseInput(m_cropRect);
            //cv::medianBlur(m_frameCropped, m_frameCropped, 3);

            equalizeIntensityInplace(m_frameCropped);
            cv::Mat cropNormalized = m_frameCropped.clone();

            /*int r = 2;
            for(int i=r; i< cropNormalized.cols - r; i++) {
                for(int j=r; j<cropNormalized.rows - r; j++) {
                    cv::Vec3f color = cropNormalized.at<cv::Vec3b>(cv::Point(i,j));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i-r,j));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i+r,j));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i,j-r));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i,j+r));

                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i-r,j-r));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i-r,j+r));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i+r,j-r));
                    color += cropNormalized.at<cv::Vec3b>(cv::Point(i+r,j+r));

                    color /= 9.0;
                    m_frameCropped.at<cv::Vec3b>(cv::Point(i,j)) = color;
                }
            }*/

            //adaptiveHistogramEqualization(m_frameCropped, 20, 4).copyTo(m_frameCropped);
            //cv::medianBlur(m_frameCropped, m_frameCropped, 3);

            //cv::medianBlur(m_frameCropped, m_frameCropped, 7);

            //cv::fastNlMeansDenoisingColoredMulti(m_frameCropped,m_frameCropped,3,3,7,7);

            if (m_blinkData.faceDetected) {
                rectangle(m_frameBaseInput, m_cropRect, cv::Scalar(255, 0, 0), 2, 1);

                putText(m_frameBaseInput, "Input Area", cv::Point(m_cropRect.x, m_cropRect.y - 10),
                        cv::FONT_HERSHEY_SIMPLEX, 1, cv::Scalar(255, 0, 0), 3);
            }

            processFrame();

            framesSinceHist++;
            auto timeSinceHist = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::high_resolution_clock::now() - lastHistTime)
                    .count();
            if (framesSinceHist >= 3 && timeSinceHist >= 600) {
                framesSinceHist = 0;
                lastHistTime = std::chrono::high_resolution_clock::now();
                showHist(m_frameBaseInputMeanGrey(m_cropRect));
                m_lowLightMode = m_cam.exposure + 0 == 1 && m_cam.gain + 0 > 0.8;
            }

            //bool eyeSizeCheck = m_frameLeftEye.size().height<=6 || m_frameRightEye.size()<=6;

            if (m_trackEyes) {
                if (m_eyeState == 0) {
                    lastBlinkAt = std::chrono::high_resolution_clock::now();
                }

                double threshold = 0.5;
                auto sencondsSinceLastBlink = std::chrono::duration_cast<std::chrono::seconds>(
                        std::chrono::high_resolution_clock::now() - lastBlinkAt)
                        .count();
                if (!eyesClosed && m_eyeState > threshold && sencondsSinceLastBlink > 0.4) {
                    blinks += 1;
                    avgBlinkTime = 0.9 * avgBlinkTime + 0.1 * sencondsSinceLastBlink;
                    lastBlinkAt = std::chrono::high_resolution_clock::now();
                    //std::cout << m_eyeState << " | " << "Blinks: " << blinks << std::endl;
                } else if (m_eyeState > 0) {
                    //std::cout << m_eyeState << std::endl;
                }

                eyesClosed = m_eyeState > threshold;
            }

            if (m_showFrames) {
                //imshow("Base Input Mean", m_frameBaseInputMean);
                imshow("Base Input", m_frameBaseInput);
                //  imshow("frameRaw3FDiff", m_frameRaw3FDiff);
                //imshow("frameRaw", m_frameRaw);
                //imshow("frameRaw3", m_frameRaw3);

                if (m_trackEyes && !m_frameEyeAreaSmooth.empty()) {
                    imshow("frameEyeAreaSmooth", m_frameEyeAreaSmooth);
                }
            }

            cv::resize(m_frameBaseInput, m_frameBaseInputScaled, cv::Size(1920, 1080), 0, 0, cv::INTER_LINEAR);

            if (m_showFrames && m_trackEyes && !m_frameLeftEye.empty()) {
                //imshow("frameLeftEyeI", m_frameRaw3FDiff(projectRectOutOfParent(m_frameLeftEyeBox, m_faceRect)));
                //imshow("frameRightEyeI", m_frameRaw3FDiff(projectRectOutOfParent(m_frameRightEyeBox, m_faceRect)));
                // cv::fastNlMeansDenoisingColored(m_frameLeftEye, m_frameLeftEye);
                // cv::fastNlMeansDenoisingColored(m_frameRightEye, m_frameRightEye);
                imshow("frameLeftEye", m_frameLeftEye);
                imshow("frameRightEye", m_frameRightEye);
            }

            m_utex.lock();
            m_blinkData.fps = static_cast<int>(fps);
            m_blinkData.blinks = blinks;
            m_blinkData.avgBlinkTime = avgBlinkTime;
            m_blinkData.faceDetected = m_framesSinceFace < m_maxFPS * 5;
            m_blinkData.eyesDetected = m_framesSinceFace < m_maxFPS;
            m_blinkData.eyesOpen = !eyesClosed;
            m_blinkData.trackEyes = m_trackEyes;
            m_blinkData.enableFiveMinBreak = m_enableFiveMinBreak;
            m_utex.unlock();
        } else {
            m_streamIsOk = false;
        }

        auto execTimeInMs = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::high_resolution_clock::now() - beginFrame)
                .count();

        double timeLeftInMs = 1000.0 / m_maxFPS - execTimeInMs;
        if (timeLeftInMs > 0) {
            usleep(static_cast<int>(timeLeftInMs * 1000));
        }

        if (frameReceived) {
            execTimeInMs = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::high_resolution_clock::now() - beginFrame)
                    .count();

            fps = 0.9 * fps + 0.1 * 1000.0 / execTimeInMs;
        }
        int k = cv::waitKey(5);
        if (k == 27) {
            cv::destroyAllWindows();
            break;
        }
    }
}

void BlinkDetector::AbstractBlinkDetector::processFrame() {
    //scale for resizing.
    const double scale =
            std::max(m_frameRaw.size().width / m_cropRect.width, m_frameRaw.size().height / m_cropRect.height) *
            m_settings.value("scale", 1).toDouble();

    cv::Mat gray = BlinkDetector::toGray(m_frameCropped);

    //resize the gray scale image for speeding the face detection.
    cv::Mat resized;
    resize(gray, resized, cv::Size(), scale, scale);

    if (!findFace(resized)) {
        if (m_framesSinceFace > m_maxFPS) {
            resetCrop();
            m_frameFace = m_frameBaseInput(m_faceRect);
        }

        m_framesSinceFace += 1;
    } else {
        m_framesSinceFace = 0;
        scaleCvRect(m_faceRect, 1 / scale, false);
        m_faceRect = projectRectOutOfParent(m_faceRect, m_cropRect);
        m_eyeAreaRect = cv::Rect(m_faceRect);
        m_eyeAreaRect.y += m_eyeAreaRect.height * 0.1;
        m_eyeAreaRect.x += m_eyeAreaRect.width * 0.05;
        m_eyeAreaRect.height *= 0.4;
        m_eyeAreaRect.width *= 0.9;
        limitRectToFrame(m_faceRect, m_frameBaseInput);
        limitRectToFrame(m_eyeAreaRect, m_frameBaseInput);
        m_frameFace = m_frameBaseInput(m_faceRect);
        m_frameEyeArea = m_frameBaseInput(m_eyeAreaRect);

        if (m_trackEyes) {
            //cv::Rect glassesBridge(0.3*m_frameEyeArea.cols,0.1*m_frameEyeArea.rows, 0.4*m_frameEyeArea.cols, 0.6*m_frameEyeArea.rows);
            cv::Rect glassesBridge(0, 0, m_frameEyeArea.cols, m_frameEyeArea.rows);

            /* begin try to remove glasses */
            if (false && !m_frameEyeArea.empty()) {
                cv::Mat dst;
                // cv::cornerHarris(toGray(m_frameEyeAreaSmooth(glassesBridge)), dst, 2, 3, 0.1, cv::BORDER_DEFAULT);
                cv::Canny(m_frameEyeArea(glassesBridge), dst, 50, 150, 3);

                //cv::Canny(m_frameEyeArea, dst, 1000, 4500, 5);

                cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
                cv::dilate(dst, dst, element);

                /* std::vector<std::vector<cv::Point>> contours;
                 std::vector<cv::Vec4i> hierarchy;
                 findContours(dst, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_SIMPLE);


                 // filter the contours
                 std::vector<std::vector<cv::Point>> filteredBlobs;
                 cv::Mat centers =cv::Mat::zeros(0,2,CV_64FC1);
                 for(int i = 0; i < contours.size(); i++) {
                     // calculate circularity
                     double area = contourArea(contours[i]);
                     double arclength = arcLength(contours[i], true);
                     double circularity = 4 * CV_PI * area / (arclength * arclength);
                     //if(area < dst.rows * dst.rows * 0.01 || circularity > 0.55 ) {
                         filteredBlobs.push_back(contours[i]);
                     //}
                 }

                 drawContours(dst, filteredBlobs, -1, cv::Scalar(100,100,100), cv::FILLED, 1);
     */

                cv::Mat z = cv::Mat::zeros(m_frameEyeArea.rows, m_frameEyeArea.cols, CV_8UC1);

                cv::Mat o = cv::Mat(m_frameEyeArea.rows, m_frameEyeArea.cols, CV_8UC1, cv::Scalar(255, 255, 255));
                o(glassesBridge).copyTo(z(glassesBridge));
                imshow("detected lines", dst);
                //cv::inpaint(m_frameEyeArea,dst,m_frameEyeArea,3,cv::INPAINT_NS);
            }

            gray = BlinkDetector::toGray(m_frameCropped);
            resize(gray, resized, cv::Size(), scale, scale);
            findFace(resized);

            m_framesSinceFace = 0;
            scaleCvRect(m_faceRect, 1 / scale, false);
            m_faceRect = projectRectOutOfParent(m_faceRect, m_cropRect);
            m_eyeAreaRect = cv::Rect(m_faceRect);
            m_eyeAreaRect.y += m_eyeAreaRect.height * 0.1;
            m_eyeAreaRect.x += m_eyeAreaRect.width * 0.05;
            m_eyeAreaRect.height *= 0.4;
            m_eyeAreaRect.width *= 0.9;
            limitRectToFrame(m_faceRect, m_frameBaseInput);
            limitRectToFrame(m_eyeAreaRect, m_frameBaseInput);
            m_frameFace = m_frameBaseInput(m_faceRect);
            m_frameEyeArea = m_frameBaseInput(m_eyeAreaRect);

            /* for(int i=0; i< m_frameEyeArea.cols; i++) {
                 for(int j=0; j<m_frameEyeArea.rows; j++) {
                     cv::Vec3b color = m_frameEyeArea.at<cv::Vec3b>(cv::Point(i,j));
                     color[0] = 0;
                     //color[1] = 0;
                     m_frameEyeArea.at<cv::Vec3b>(cv::Point(i,j)) = color;
                 }
             }*/

            //cv::Mat one = cv::Mat(dst.rows, dst.cols, CV_8UC3, cv::Scalar(255,255,255));
            //one.copyTo(m_frameEyeArea, dst);
            /* end try to remove glasses */

            int windowSize = 1;
            if (!m_eyeAreaTimeShift.empty()) {
                if (m_eyeAreaTimeShift.size() > windowSize - 1) {
                    m_eyeAreaTimeShift.pop_front();
                }
            } else {
                // std::cout<<"clear|"<<!m_eyeAreaTimeShift.empty()<<"|"<<m_frameEyeArea.cols<<"|"<<m_frameEyeArea.rows<<"|"<<m_eyeAreaTimeShift.front().cols<<"|"<<m_eyeAreaTimeShift.front().rows;
                // std::cout<<"|"<<(m_frameEyeArea.cols==m_eyeAreaTimeShift.front().cols)<<"|"<<(m_frameEyeArea.rows==m_eyeAreaTimeShift.front().rows)<<std::endl;
                //m_eyeAreaTimeShift.clear();
            }

            //m_eyeAreaTimeShift.push_back(m_frameEyeArea.clone());

            bool sameSize = false;
            for (int i = 1; i < m_eyeAreaTimeShift.size(); i++) {
                sameSize &= m_eyeAreaTimeShift.at(0).size == m_eyeAreaTimeShift.at(i).size;
            }

            if (sameSize && m_eyeAreaTimeShift.size() >= windowSize && windowSize > 1) {
                std::vector<cv::Mat> cropTimeShiftAsVector = {m_eyeAreaTimeShift.begin(), m_eyeAreaTimeShift.end()};
                cv::fastNlMeansDenoisingMulti(cropTimeShiftAsVector, m_frameEyeAreaSmooth,
                                              (m_eyeAreaTimeShift.size() - 1) / 2, m_eyeAreaTimeShift.size(), /*h*/ 10,
                                              3, 7);
                m_frameFace = m_frameBaseInput(m_faceRect);
                m_frameEyeAreaSmooth.copyTo(m_frameEyeArea);
                rectangle(m_frameBaseInput, m_eyeAreaRect, cv::Scalar(255, 255, 0), 1, 1);
            } else {
                m_frameEyeAreaSmooth = m_frameEyeArea;
            }

            /*cv::Mat oldR = m_frameRightEye.clone();
            cv::Mat oldL = m_frameLeftEye.clone();*/

            findEyes();

            /*cv::InputArrayOfArrays l = {oldL};
            cv::InputArrayOfArrays r = {oldR};*/

            /*if(!oldL.empty() && !oldR.empty()) {
                cv::fastNlMeansDenoisingMulti(l, m_frameRightEye, 2, 5, 4, 7, 35);
                cv::fastNlMeansDenoisingMulti(r, m_frameLeftEye, 2, 5, 4, 7, 35);
            }*/

            //m_frameRightEye = BlinkDetector::equalizeIntensity(m_frameRightEye);
            //m_frameLeftEye = BlinkDetector::equalizeIntensity(m_frameLeftEye);

            double widthCutOff = m_lowLightMode ? 0.26 : 0.3;
            // TODO replace with static casts
            for (auto &rightEyeMark : m_rightEyeMarks) {
                rightEyeMark.x -= (int) (widthCutOff * m_frameRightEye.cols);
            }
            for (auto &leftEyeMark : m_leftEyeMarks) {
                leftEyeMark.x -= (int) (widthCutOff * m_frameLeftEye.cols);
            }

            m_frameLeftEyeBox.x += (int) (widthCutOff * m_frameLeftEyeBox.width);
            m_frameLeftEyeBox.width -= (int) (2.0 * widthCutOff * m_frameLeftEyeBox.width);

            m_frameRightEyeBox.x += (int) (widthCutOff * m_frameRightEyeBox.width);
            m_frameRightEyeBox.width -= (int) (2.0 * widthCutOff * m_frameRightEyeBox.width);

            m_frameRightEye = m_frameRightEye(
                    cv::Rect((int) (widthCutOff * m_frameRightEye.cols), 0,
                             (int) (m_frameRightEye.cols * (1 - 2.0 * widthCutOff)), m_frameRightEye.rows));
            m_frameLeftEye = m_frameLeftEye(
                    cv::Rect((int) (widthCutOff * m_frameLeftEye.cols), 0,
                             (int) (m_frameLeftEye.cols * (1 - 2.0 * widthCutOff)), m_frameLeftEye.rows));

            //cv::GaussianBlur(m_frameRightEye, m_frameRightEye, cv::Size(5, 5), 0, 0);
            //cv::GaussianBlur(m_frameLeftEye, m_frameLeftEye, cv::Size(5, 5), 0, 0);

            calcEyeState();
            //std::cout << m_eyeState<<std::endl;

            /*
            for (const auto &m_leftEyeMark : m_leftEyeMarks) {
                circle(
                        m_frameBaseInput,
                        m_leftEyeMark + cv::Point(m_faceRect.x, m_faceRect.y) +
                        cv::Point(m_frameLeftEyeBox.x, m_frameLeftEyeBox.y), 1,
                        cv::Scalar(0, 0, 255),
                        1,
                        1
                );
            }

            for (const auto &m_rightEyeMark : m_rightEyeMarks) {
                circle(
                        m_frameBaseInput,
                        m_rightEyeMark + cv::Point(m_faceRect.x, m_faceRect.y) +
                        cv::Point(m_frameRightEyeBox.x, m_frameRightEyeBox.y), 1,
                        cv::Scalar(0, 0, 255),
                        1,
                        1
                );
            }
    */
        }

        rectangle(m_frameBaseInput, m_faceRect, cv::Scalar(0, 255, 0), 1, 1);
        putText(m_frameBaseInput, "Face Area", cv::Point(m_faceRect.x, m_faceRect.y - 10), cv::FONT_HERSHEY_SIMPLEX,
                0.8, cv::Scalar(0, 255, 0), 2);

        calcInputRect();
    }
}

void BlinkDetector::AbstractBlinkDetector::calcInputRect() {

    m_oldFaceRect.x = std::abs(m_faceRect.x - m_oldFaceRect.x) > m_inShift ? m_faceRect.x : m_oldFaceRect.x;
    m_oldFaceRect.y = std::abs(m_faceRect.y - m_oldFaceRect.y) > m_inShift ? m_faceRect.y : m_oldFaceRect.y;
    m_oldFaceRect.width = std::abs(m_faceRect.x + m_faceRect.width - m_oldFaceRect.width - m_oldFaceRect.x) > m_inShift
                          ? m_faceRect.width
                          : m_oldFaceRect.width;
    m_oldFaceRect.height =
            std::abs(m_faceRect.y + m_faceRect.height - m_oldFaceRect.height - m_oldFaceRect.y) > m_inShift
            ? m_faceRect.height
            : m_oldFaceRect.height;

    m_cropRect = cv::Rect(m_oldFaceRect);

    m_cropRect.x -= (int) (m_cropRect.width * (m_inputCropScale - 1.0) / 2.0);
    m_cropRect.y -= (int) (m_cropRect.height * (m_inputCropScale - 1.0) / 2.0);

    int newX = roundToNextMultipleOf(m_cropRect.x, true, m_grid);
    int newY = roundToNextMultipleOf(m_cropRect.y, true, m_grid);

    m_cropRect.width = roundToNextMultipleOf(m_cropRect.x + m_cropRect.width * m_inputCropScale, false, m_grid) - newX;
    m_cropRect.height =
            roundToNextMultipleOf(m_cropRect.y + m_cropRect.height * m_inputCropScale, false, m_grid) - newY;

    m_cropRect.x = newX;
    m_cropRect.y = newY;

    limitRectToFrame(m_cropRect, m_frameBaseInput);
}

bool BlinkDetector::AbstractBlinkDetector::copyData(BlinkData *data) {
    m_utex.lock();

    bool changed =
            // TODO overload == operator of BlinkData struct
            data->fps != round(m_blinkData.fps) ||
            data->avgBlinkTime != std::round(m_blinkData.avgBlinkTime * 10) / 10.0 ||
            data->blinks != m_blinkData.blinks || data->faceDetected != m_blinkData.faceDetected ||
            data->eyesDetected != m_blinkData.eyesDetected || data->eyesOpen != m_blinkData.eyesOpen;
    if (changed) {
        data->fps = static_cast<int>(std::round(m_blinkData.fps));
        data->blinks = m_blinkData.blinks;
        data->avgBlinkTime = std::round(m_blinkData.avgBlinkTime * 10) / 10.0;
        data->faceDetected = m_blinkData.faceDetected;
        data->eyesDetected = m_blinkData.eyesDetected;
        data->eyesOpen = m_blinkData.eyesOpen;
        data->trackEyes = m_blinkData.trackEyes;
        data->enableFiveMinBreak = m_blinkData.enableFiveMinBreak;
    }
    m_utex.unlock();

    return changed;
}

void BlinkDetector::AbstractBlinkDetector::checkSource() {
    if (!m_source.isOpened() || !m_streamIsOk) {
        std::cout << "opening video source... " << std::endl;
        QString videoInput = m_settings.value("videoSource", "").toString();
        if (videoInput != "") {
            m_source = cv::VideoCapture(videoInput.toUtf8().constData());
        } else {
            m_source = cv::VideoCapture(0);
        }

        m_streamIsOk = m_source.isOpened();

        if (m_streamIsOk) {
            std::cout << "done" << std::endl;
        } else {
            std::cout << "FAIL" << std::endl;
            sleep(1);
        }
    }
}
