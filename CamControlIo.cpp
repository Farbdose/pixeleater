//
// Created by fabian on 06.04.19.
//

#include "CamControlIo.h"

int BlinkDetector::CamControlIo::getMin() const {
    return m_query.minimum;
}

int BlinkDetector::CamControlIo::getDef() const {
    return m_query.default_value;
}

int BlinkDetector::CamControlIo::getMax() const {
    return m_query.maximum;
}

int BlinkDetector::CamControlIo::getValAbs() const {
    return m_ctrl.value;
}

float BlinkDetector::CamControlIo::getRelativeStep() const {
    return 1.0f / (float) (m_query.maximum - m_query.minimum) / m_query.step;
}

float BlinkDetector::CamControlIo::getVal() const {
    return (float) (m_ctrl.value - m_query.minimum) / (float) (m_query.maximum - m_query.minimum);
}

void BlinkDetector::CamControlIo::setValAbs(const int value) {
    m_ctrl.value = value;
    ioctl(m_camera_fd, VIDIOC_S_CTRL, &m_ctrl);
}

BlinkDetector::CamControlIo::CamControlIo(const int &camera_fd, __u32 id) {
    if (camera_fd) {
        m_camera_fd = camera_fd;
        m_query.id = m_ctrl.id = id;

        ioctl(camera_fd, VIDIOC_QUERYCTRL, &m_query);
        setValAbs(m_query.default_value);

        m_precission = ceil(log(1.0f / (float) ((m_query.maximum - m_query.minimum)) / m_query.step) / log(0.01));
    }
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator=(const float &value) {
    __s32 old = m_ctrl.value;
    m_ctrl.value = static_cast<__s32>(m_query.minimum +
                                      (((m_query.maximum - m_query.minimum) * (std::max(0.0f, std::min(1.0f, value))) +
                                        m_query.step / 2.0) / m_query.step) * m_query.step);

    double ten = pow(10, m_precission);
    m_ctrl.value = static_cast<__s32>(round(m_ctrl.value * ten) / ten);

    if (old != m_ctrl.value) {
        ioctl(m_camera_fd, VIDIOC_S_CTRL, &m_ctrl);
    }

    return *this;
}

BlinkDetector::CamControlIo::operator float() const {
    return getVal();
}

BlinkDetector::CamControlIo::operator double() const {
    return getVal();
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator*=(const float &value) {
    operator=(getVal() * value);
    return *this;
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator/=(const float &value) {
    operator=(getVal() / value);
    return *this;
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator+=(const float &value) {
    operator=(getVal() + value);
    return *this;
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator-=(const float &value) {
    operator=(getVal() - value);
    return *this;
}

float BlinkDetector::CamControlIo::operator+(const float &value) {
    return getVal() + value;
}

float BlinkDetector::CamControlIo::operator-(const float &value) {
    return getVal() - value;
}

void BlinkDetector::CamControlIo::setMaximum(int value) {
    m_query.maximum = value;
    m_precission = ceil(log(1.0f / (float) ((m_query.maximum - m_query.minimum)) / m_query.step) / log(0.1));
}

void BlinkDetector::CamControlIo::setMinimum(int value) {
    m_query.minimum = value;
    m_precission = ceil(log(1.0f / (float) ((m_query.maximum - m_query.minimum)) / m_query.step) / log(0.1));
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator--() {
    setValAbs(getValAbs() - m_query.step);
    return *this;
}

BlinkDetector::CamControlIo &BlinkDetector::CamControlIo::operator++() {
    setValAbs(getValAbs() + m_query.step);
    return *this;
}

BlinkDetector::CamControlIo BlinkDetector::CamControlIo::operator--(int x) &{
    BlinkDetector::CamControlIo temp = *this;
    ++*this;
    return temp;
}

BlinkDetector::CamControlIo BlinkDetector::CamControlIo::operator++(int x) &{
    BlinkDetector::CamControlIo temp = *this;
    --*this;
    return temp;
}

bool BlinkDetector::CamControlIo::operator<(const float &value) {
    return getVal() < value;
}

bool BlinkDetector::CamControlIo::operator>(const float &value) {
    return getVal() > value;
}

bool BlinkDetector::CamControlIo::operator<(const CamControlIo &value) {
    return getVal() < value.getVal();
}

bool BlinkDetector::CamControlIo::operator>(const CamControlIo &value) {
    return getVal() > value.getVal();
}

float BlinkDetector::operator-(const float x, const BlinkDetector::CamControlIo &y) {
    return x - y.getVal();
}

float BlinkDetector::operator+(const float x, const BlinkDetector::CamControlIo &y) {
    return x + y.getVal();
}

bool BlinkDetector::CamControlIo::operator==(const float &value) {
    return getVal() == value;
}

bool BlinkDetector::CamControlIo::operator==(const BlinkDetector::CamControlIo &value) {
    return getVal() == value.getVal();
}
