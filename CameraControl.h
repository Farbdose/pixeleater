//
// Created by fabian on 06.04.19.
//
#pragma once

#include "CamControlIo.h"

namespace BlinkDetector {

    class CameraControl {
    public:
        explicit CameraControl(const int &camera_fd);

        CameraControl() = default;

        CamControlIo gamma;
        CamControlIo gain;
        CamControlIo exposure;
        CamControlIo contrast;
    };
}
