#include <utility>

#include "dlibBlinkDetector.h"

namespace BlinkDetector {

    DlibBlinkDetector::DlibBlinkDetector(const QString &settingsPath)
            : m_alpha(350), m_beta(5), m_darkest(10), m_darkness(70), m_brightness(100),
              AbstractBlinkDetector(settingsPath) {
    }

    void DlibBlinkDetector::loadModels() {
        // Load face detection and pose estimation models.
        m_detector = dlib::get_frontal_face_detector();
        dlib::deserialize(m_settings.value("faceLandmarks", "./models/shape_predictor_68_face_landmarks.dat")
                                  .toString()
                                  .toUtf8()
                                  .constData())
                >> m_posemodel;

        if (m_showFrames && m_trackEyes) {
            cv::createTrackbar("alpha", "frameLeftEye", &m_alpha, 500, nullptr);
            cv::createTrackbar("beta", "frameLeftEye", &m_beta, 100, nullptr);
            cv::createTrackbar("darkness", "frameLeftEye", &m_darkness, 255, nullptr);
            cv::createTrackbar("darkest", "frameLeftEye", &m_darkest, 255, nullptr);
            cv::createTrackbar("brightness", "frameLeftEye", &m_brightness, 255, nullptr);
        }
    }

    bool BlinkDetector::DlibBlinkDetector::findFace(const cv::Mat frame) {

        // Cover OpenCV image Dlib image i.e. cimg
        dlib::cv_image<unsigned char> cimg(frame);

        // Detect faces using DLib
        std::vector<dlib::rectangle> face_DLib = m_detector(cimg);

        if (!face_DLib.empty()) {
            m_faceRect.x = static_cast<int>(face_DLib[0].left());
            m_faceRect.y = static_cast<int>(face_DLib[0].top());
            m_faceRect.width = static_cast<int>(face_DLib[0].width());
            m_faceRect.height = static_cast<int>(face_DLib[0].height());
            return true;
        } else {
            return false;
        }
    }

    void BlinkDetector::DlibBlinkDetector::findEyes() {
        //converts original image to gray scale and stores it in "gray".
        cv::Mat gray = m_frameBaseInputMeanGrey.getMat(cv::ACCESS_READ)(m_faceRect);

        std::vector<cv::Point> landmarks;
        auto shape = m_posemodel(dlib::cv_image<unsigned char>(gray),
                                 dlib::rectangle(0, 0, m_frameFace.size().width, m_frameFace.size().height));
        dlib_point2cv_Point(shape, landmarks);

        m_leftEyeMarks.clear();
        m_rightEyeMarks.clear();

        int eyes2[] = {36, 37, 38, 39, 40, 41};
        for (int s : eyes2) {
            m_leftEyeMarks.push_back(landmarks[s] /*+ cv::Point(m_faceRect.x, m_faceRect.y)*/);
        }

        int eyes3[] = {45, 44, 43, 42, 47, 46};
        for (int s : eyes3) {
            m_rightEyeMarks.push_back(landmarks[s] /* + cv::Point(m_faceRect.x, m_faceRect.y)*/);
        }

        m_frameLeftEyeBox = cv::boundingRect(m_leftEyeMarks);
        m_frameRightEyeBox = cv::boundingRect(m_rightEyeMarks);

        scaleCvRect(m_frameLeftEyeBox, 1.8, true);
        scaleCvRect(m_frameRightEyeBox, 1.8, true);

        limitRectToFrame(m_frameLeftEyeBox, m_frameFace);
        limitRectToFrame(m_frameRightEyeBox, m_frameFace);

        m_frameLeftEye = m_frameFace(m_frameLeftEyeBox).clone();
        m_frameRightEye = m_frameFace(m_frameRightEyeBox).clone();

        for (auto &m_leftEyeMark : m_leftEyeMarks) {
            m_leftEyeMark -= cv::Point(m_frameLeftEyeBox.x, m_frameLeftEyeBox.y);
        }

        for (auto &m_rightEyeMark : m_rightEyeMarks) {
            m_rightEyeMark -= cv::Point(m_frameRightEyeBox.x, m_frameRightEyeBox.y);
        }
    }

    double DlibBlinkDetector::colorBased(cv::Mat &frame, std::vector<cv::Point> &shape) {
        // calculate means
        std::vector<cv::Point> shapeL = {shape[0], shape[1], shape[5]};
        cv::Scalar meanOut = getMeanOfShape(frame, shapeL);

        std::vector<cv::Point> shapeC = {shape[1], shape[2], shape[4], shape[5]};
        cv::Scalar meanC = getMeanOfShape(frame, shapeC);

        std::vector<cv::Point> shapeR = {shape[2], shape[3], shape[4]};
        meanOut += getMeanOfShape(frame, shapeR);
        meanOut /= 2.0;

        double greyC = (meanC.val[0] + meanC.val[1] + meanC.val[2]) / 3.0;
        double greyOut = (meanOut.val[0] + meanOut.val[1] + meanOut.val[2]) / 3.0;

        double res = greyC / greyOut;

        // draw translated landmarks in new frame
        /*for (const auto &i : shape) {
                cv::circle(frame, i, 1, cv::Scalar(0, 0, 255), 1, 1);
            }*/

        return res;
    }

    double DlibBlinkDetector::absColorBased(cv::Mat &frame) {
        double pixels = 0;
        double pivot = 0.3;
        int limit = 70;

        for (int i = 0; i < frame.rows; i++) {
            for (int j = 0; j < frame.cols; j++) {
                auto &color = frame.at<cv::Vec3b>(i, j);
                // double mean = cv::mean(color)[0];

                //std::cout<<cv::mean(color)<<std::endl;
                if (color[2] < limit) {
                    pixels += 1;
                }
            }
        }

        double percentage = pixels / (frame.cols * frame.rows);
        //std::cout<<pixels<<"|"<<percentage<<std::endl;

        if (percentage > pivot) {
            return 0.5 * (1 - percentage) / (1 - pivot);
        } else {
            return 0.5 + 0.5 * percentage / pivot;
        }
    }

    double DlibBlinkDetector::circleBased(cv::Mat &frame, std::vector<cv::Point> &shape, bool lowLight) {
        if (frame.empty()) {
            return 0;
        }

        double min_size = 0.3;

        /*if (lowLight) {
                brightness = 70;
                darkness = 50;
                alpha = 10;
                beta = 6;
                min_size = 0.38;
            }*/

        std::vector<cv::Vec3f> circles;
        std::vector<double> means;

        cv::HoughCircles(toGray(frame), circles, cv::HOUGH_GRADIENT, 1, 1, m_alpha, m_beta,
                         (int) (frame.rows * min_size), (int) (frame.rows * 0.5));

        cv::Mat f = frame;

        for (auto &i : circles) {
            cv::Point center(cvRound(i[0]), cvRound(i[1]));
            int radius = cvRound(i[2]);

            cv::Mat roi = cv::Mat::zeros(cv::Size(frame.cols, frame.rows), CV_8U);
            cv::circle(roi, center, radius, cv::Scalar(255, 255, 255), cv::FILLED, 8, 0);
            double mean = cv::sum(cv::mean(f, roi))[0] / f.channels();

            roi = cv::Mat::zeros(cv::Size(frame.cols, frame.rows), CV_8U);
            cv::circle(roi, center, radius + 2, cv::Scalar(255, 255, 255), 2, 8, 0);
            double outerMean = cv::sum(cv::mean(f, roi))[0] / f.channels();

            //std::cout << mean << "|" << outerMean << std::endl;
            means.push_back(mean < m_darkness && outerMean > m_brightness ? outerMean - mean : -1);
            //means.push_back(cv::sum(mean)[0] / std::max(1.0, mean.rows - 1.0));
        }

        if (!circles.empty()) {
            int darkestCircleIndex = std::max_element(means.begin(), means.end()) - means.begin();
            double cirlcDarkness = means[darkestCircleIndex];
            // std::cout<<cirlcDarkness<<std::endl;

            for (auto &i2 : circles) {
                cv::Point center(cvRound(i2[0]), cvRound(i2[1]));
                int radius = cvRound(i2[2]);
                cv::circle(frame, center, radius, cv::Scalar(0, 0, 255), 1, 8, 0);
            }

            if (cirlcDarkness != -1 /* || (255 - cirlcDarkness) < darkness*/) {
                auto i = circles[darkestCircleIndex];
                cv::Point center(cvRound(i[0]), cvRound(i[1]));
                int radius = cvRound(i[2]);
                cv::circle(frame, center, radius, cv::Scalar(0, 255, 0), 2, 8, 0);

                return 0.01;
            }
        }

        return 0.99;
    }

    double DlibBlinkDetector::edgeBased(cv::Mat &frame, std::vector<cv::Point> &shape, bool lowLight) {

        cv::Mat edges;
        cv::Rect r;
        double m;
        if (lowLight) {
            r = cv::Rect(0.2 * frame.cols, 0.3 * frame.rows, 0.3 * frame.cols, 0.7 * frame.rows);
            cv::Canny(frame, edges, 300, 500, 3);
            //cv::Canny(frame, edges, 1000, 6000, 5);
            cv::Scalar mean = cv::mean(edges(r));
            m = cv::sum(mean)[0] / std::max(1.0, mean.rows - 1.0);
            rectangle(edges, r, cv::Scalar(100, 100, 100), 1, 1);
            imshow("l", edges);
            // std::cout<<m<<std::endl;
        } else {
            r = cv::Rect(0.5 * frame.cols, 0.3 * frame.rows, 0.3 * frame.cols, 0.7 * frame.rows);
            cv::Mat image;
            cv::GaussianBlur(frame, image, cv::Size(0, 0), 3);
            cv::addWeighted(frame, 1.5, image, -0.5, 0, image);
            cv::Canny(image, edges, 20, 100, 3);
            //cv::Canny(frame, edges, 300, 500, 3);
            //cv::Canny(frame, edges, 1000, 6000, 5);
            cv::Scalar mean = cv::mean(edges(r));
            m = cv::sum(mean)[0] / std::max(1.0, mean.rows - 1.0);
            rectangle(edges, r, cv::Scalar(100, 100, 100), 1, 1);
            imshow("r", edges);
            // std::cout<<m<<std::endl;
        }

        return m < 0.1 ? 0.99 : 0.01;
    }

    void DlibBlinkDetector::calcEyeState() {
        double lQ = absColorBased(m_frameLeftEye);
        double rQ = absColorBased(m_frameRightEye);
        double absState = std::max(lQ, rQ);

        m_frameRightEye = BlinkDetector::equalizeIntensity(m_frameRightEye);
        m_frameLeftEye = BlinkDetector::equalizeIntensity(m_frameLeftEye);

        lQ = circleBased(m_frameLeftEye, m_leftEyeMarks, m_lowLightMode);
        rQ = circleBased(m_frameRightEye, m_rightEyeMarks, m_lowLightMode);
        double circleState = std::max(lQ, rQ);

        // std::cout << circleState << std::endl;

        if (lQ == rQ) {
            if (std::max(lQ, rQ) > 0.5 && m_eyeState < 0.5) {
                std::cout << "blink" << std::endl;
            }
            m_eyeState = std::max(lQ, rQ);
        }
        //m_eyeState = (0*absState + 2*circleState) * 0.5;

        return;

        if (false && m_lowLightMode) {
            if (std::max(lQ, rQ) > 0.5) {
                m_eyeState = 1;
            } else {
                lQ = absColorBased(m_frameLeftEye);
                rQ = absColorBased(m_frameRightEye);
                m_eyeState = std::max(lQ, rQ);
            }
        } else if (false) {
            m_eyeState = std::min(lQ, rQ);
        } else if (false) {
            lQ = edgeBased(m_frameLeftEye, m_leftEyeMarks, true);
            rQ = edgeBased(m_frameRightEye, m_rightEyeMarks, false);
            m_eyeState = std::min(lQ, rQ);
        } else if (true) {
        } else {
            lQ = colorBased(m_frameLeftEye, m_leftEyeMarks);
            rQ = colorBased(m_frameRightEye, m_rightEyeMarks);
            m_eyeState = (lQ + rQ) / 2.0;
        }
    }
}