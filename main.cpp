#include <QApplication>
#include <QDebug>
#include <QGuiApplication>
#include <QScreen>
#include <QWindow>
#include <dlibBlinkDetector.h>
#include <pixeleater.h>

bool file_exist(const char *fileName) {
    std::ifstream infile(fileName);
    return infile.good();
}

int main(int argc, char **argv) {
    qDebug() << "Preparing";

    QApplication application(argc, argv);

    QString homedir = getenv("HOME");
    QString settingsPath(file_exist("./PixelEater.ini") ? "./PixelEater.ini" : (homedir + "/.config/PixelEater.ini"));

    auto *blinkDetector = new BlinkDetector::DlibBlinkDetector(settingsPath);

    for (auto *screen : QGuiApplication::screens()) {
        if (screen->geometry().left() < 4000) {
            qDebug() << "Spawning PixelEater using settingsPath " << settingsPath;
            auto *pixelEater = new PixelEater::QtPixelEater(screen, blinkDetector, settingsPath);
            pixelEater->show();
            pixelEater->windowHandle()->setScreen(screen);
            pixelEater->move(screen->geometry().left(), screen->geometry().top());
        }
    }

    qDebug() << "Starting";

    int status = QApplication::exec();
    blinkDetector->stop();

    return status;
}
