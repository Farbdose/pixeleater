import matplotlib.pyplot as plt
import numpy as np

x = np.array(range(60 * 60 * 10))
y = [1]
for k in x[:-1]:
    y.append(y[-1] + min(2, 0.25 + 0.0001 * y[-1]))

y = np.array(y)

x = x / 60
y = y / 60

# Create the plot
plt.plot(x, y)

# Show the plot
plt.show()
