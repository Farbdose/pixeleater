//
// Created by fabian on 23.03.19.
//
#pragma once

#include "CameraControl.h"
#include <QtCore/QSettings>
#include <cmath>
#include <deque>
#include <fcntl.h>
#include <libv4l2.h>
#include <linux/v4l2-controls.h>
#include <mutex>
#include <opencv2/core.hpp>
#include <opencv2/opencv_modules.hpp>
#include <opencv2/videoio.hpp>
#include <thread>

namespace BlinkDetector {

// TODO move as public static into classes based on context
    void toggleButtonCallback(int state, void *user);

    cv::Mat adaptiveHistogramEqualization(const cv::Mat &input, int clipLimit, int tileSize);

    cv::Mat equalizeIntensity(const cv::Mat &inputImage);

    void equalizeIntensityInplace(const cv::Mat &inputImage);

    cv::Mat toGray(const cv::Mat &src);

    cv::UMat toGray(const cv::UMat &src);

    int roundToNextMultipleOf(double num, bool down, int grid);

    cv::Rect projectRectOutOfParent(const cv::Rect &rect, const cv::Rect &parent);

    void scaleCvRect(cv::Rect &rect, double scale, bool fromCenter);

    int limitIntToRange(int min, int num, int max);

    void limitRectToFrame(cv::Rect &rect, const cv::Mat &frame);

    cv::Scalar getMeanOfShape(cv::Mat &frame, std::vector<cv::Point> &shape);

    double EAR(std::vector<cv::Point> &landmarks);

    struct OptimizedHistBatch {
        int cursor;
        cv::Mat1f input;
        cv::Mat1f output;
    };

    struct HistData {
        float error;
        cv::Mat hist;
        cv::Mat1f input;
        cv::Mat1f output;
        //        median;
        //        varianz;
        //        skewness;
        //        minValue;
        //        minValueHeight;
        //        maxValue;
        //        maxValueHeight;
    };

    struct BlinkData {
        int fps;
        bool faceDetected;
        bool trackEyes;
        int blinks;
        double avgBlinkTime;
        bool eyesDetected;
        bool eyesOpen;
        bool enableFiveMinBreak;
    };

    class AbstractFaceProcessor;

    class AbstractEyeCalculator;

// TODO rename to BlinkController
    class AbstractBlinkDetector {

    public:
        explicit AbstractBlinkDetector(
                const QString &settingsPath);

        cv::Mat m_frameBaseInputScaled;

        bool copyData(BlinkData *data);

        void stop();

    protected:
        void calcHist(const cv::UMat &src, int histSize);

        void checkSource();

        void calcInputRect();

        void resetCrop();

        void run();

        void showHist(const cv::UMat &src);

        void processFrame();

        // TODO move into separate abstract class
        virtual void calcEyeState() = 0;

        // TODO move into separate class AbstractFaceProcessor with children DlibFaceProcessor and DnnFaceProcessor
        virtual void loadModels() = 0;

        virtual bool findFace(cv::Mat frame) = 0;

        virtual void findEyes() = 0;

        QSettings m_settings;
        bool m_running;
        std::thread m_ainLoop;
        int m_framesSinceFace;
        bool m_devMode;

        BlinkData m_blinkData;

        double m_eyeState;
        int m_grid;
        int m_inShift;
        size_t m_batchSize;
        double m_inputCropScale;
        int m_maxFPS;

        HistData m_hist;
        OptimizedHistBatch m_histBatch;

        std::mutex m_utex;
        cv::Rect m_cropRect;
        cv::Rect m_faceRect;
        cv::Rect m_oldFaceRect;
        cv::Rect m_eyeAreaRect;
        cv::Mat m_frameBaseInput;
        cv::UMat m_frameBaseInputMean;
        cv::UMat m_frameBaseInputMeanGrey;
        cv::UMat m_frameRaw2;
        cv::UMat m_frameRaw3FDiff;
        cv::UMat m_frameRaw3;
        cv::Mat m_frameFace;
        cv::Mat m_frameEyeArea;
        cv::Mat m_frameEyeAreaSmooth;
        cv::UMat m_frameRaw;
        cv::Mat m_frameRawHist;
        cv::Mat m_frameCropped;
        cv::Mat m_frameLeftEye;
        cv::Mat m_frameRightEye;
        cv::Rect m_frameLeftEyeBox;
        cv::Rect m_frameRightEyeBox;
        std::deque<cv::Mat> m_eyeAreaTimeShift;
        std::vector<cv::Point> m_leftEyeMarks;
        std::vector<cv::Point> m_rightEyeMarks;

        // TODO move hist calc in separate class
        // TODO move cam control into separate class using hist calc class
        CameraControl m_cam;
        cv::VideoCapture m_source;
        bool m_streamIsOk;
        bool m_lowLightMode;
        bool m_trackEyes;
        bool m_showFrames;
        int m_num;
        bool m_enableFiveMinBreak;
    };
}
