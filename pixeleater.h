#pragma once

#include "AbstractBlinkDetector.h"
#include <QDebug>
#include <QFrame>
#include <QMainWindow>
#include <QScreen>
#include <QTimer>
#include <QVBoxLayout>
#include <QWidget>
#include <QtConcurrent/QtConcurrent>
#include <QtGui/QFontDatabase>
#include <QtGui/QPainter>
#include <QtWidgets/QGraphicsPixmapItem>
#include <QtWidgets/QGraphicsView>
#include <opencv2/opencv.hpp>

namespace PixelEater {

    class QtPixelEater : public QGraphicsView {
    Q_OBJECT;

    public:
        QtPixelEater(QScreen *scr, BlinkDetector::AbstractBlinkDetector *detector, const QString &settingsPath);

    protected:
        QSettings m_settings;
        QGraphicsPixmapItem m_videoStream;
        QGraphicsPixmapItem m_flash_black;
        QGraphicsPixmapItem m_flash_white;
        QGraphicsTextItem m_statusText;
        QTimer m_timer;
        QScreen *m_screen;
        QGraphicsPixmapItem m_background;
        BlinkDetector::AbstractBlinkDetector *m_detector;
        BlinkDetector::BlinkData m_blinkData;

        bool m_devMode;
        int m_counter;
        int m_ticksPerSec;
        int m_secondsSinceEyesClosed;
        double m_eyeTimeTotal;
        double m_eyeTime;
        double m_breakTimeDone;
        double m_breakTimeRequired;
        double m_breakTimeLeft;

        double calcBreakTime(double x);

        void flash();

        void setup();

        void setupScene();

        void tick();
    };
}
